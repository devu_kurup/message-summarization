import spacy
from spacy.matcher import Matcher
nlp = spacy.load("en_core_web_sm")
f = open("message.txt","r")
content = f.read()
doc = nlp(content)
pattern1 = [[{'LOWER': 'between'},
           {'POS':'PROPN'},
           {'LOWER': 'and'},
           {'POS': 'PROPN'}],
           [{'LOWER': 'charge'},
           {'POS':'PROPN'},
           {'pos':'NUM'},
           {'LOWER': 'for'},
           {'LOWER': 'cancel'},
           {'IS_PUNCT': True} ,
           {'LOWER': 'refund'},
           {'IS_PUNCT': True}],
           [{'LOWER': 'FCL'},
            {'IS_PUNCT': True},
            {'POS':'PROPN'},
            {'IS_PUNCT':True}],
          [{'LOWER': 'charge'},
           {'POS':'PROPN'},
           {'pos':'NUM'},
           {'LOWER': 'for'},
           {'LOWER': 'no'},
           {'IS_PUNCT': True} ,
           {'LOWER': 'show'},
           {'IS_PUNCT': True}],
          [{'LOWER': 'charge'},
           {'POS':'PROPN'},
           {'pos':'NUM'},
           {'LOWER': 'for'},
           {'LOWER': 'reissue'},
           {'IS_PUNCT': True} ,
           {'LOWER': 'revalidation'},
           {'IS_PUNCT': True}]]
matcher = Matcher(nlp.vocab)
matcher.add("matching_1", pattern1)

matches = matcher(doc)
for matchid,start,end in matches:
    span = doc[start:end]
    print(span.text)